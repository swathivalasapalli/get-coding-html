const rock = document.getElementById('rock');
rock.addEventListener('click', () =>{
    // console.log("you clicked rock")
    game('rock');
})

const paper = document.getElementById('paper');
paper.addEventListener('click', () => {
    // console.log("you clicked paper");
    game('paper');
});

const scissor = document.getElementById('scissor');
scissor.addEventListener('click', () => {
    // console.log("you clicked scissors");
    game('scissor');
});

const userScore = 0;
const computerScore = 0;
const user = document.getElementById('user');
const computer = document.getElementById("comp");
const scoreBoard = document.querySelector("board");

const getComputerChoice = () =>{
    const choices = ['rock', 'paper', 'scissors'];
    const randomnumber = Math.floor(Math.random() * 3);
    return choices[randomnumber];
}

const game = (userChoice) => {
    const computerChoice = getComputerChoice();
    console.log("userchoice is" + userChoice);
    console.log("computer choice is" + computerChoice);
}
